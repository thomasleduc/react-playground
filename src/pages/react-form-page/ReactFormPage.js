import React from 'react';
import { Form, Text } from 'react-form';

import '../../styles/forms.css';
import './ReactFormPage.css';
import {validateHello} from '../../utils/form-validation/hello.validator';

const ReactForm = () => {
  return (
    <div>
      <Form render={
        formApi => (
          <div>
            <form onSubmit={formApi.submitForm} id="react-form" className="Form ReactForm">
              <label htmlFor="hello">Hello World</label>
              <Text field="hello" id="hello" validate={validateHello} />
              <button type="submit">Submit</button>
              <button onClick={formApi.resetAll}>Reset</button>
              <div className="Form-errors">
                {Object.values(formApi.errors || {}).map(error => <p>{error}</p>)}
              </div>
            </form>
            <pre className="ReactForm-DebugFormApi">
              {JSON.stringify(formApi, 0, 2)}
            </pre>
          </div>
        )}
      />
    </div>
  );
};

export default ReactForm;
