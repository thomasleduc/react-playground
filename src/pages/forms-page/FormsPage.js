import React from 'react';
import {Link} from 'react-router-dom';
import {RouteWithSubRoutes} from '../../utils/router';

import './FormsPage.css';

export default ({ routes }) => (
  <div>
    <h2>Forms</h2>
    <div className="nav">
      <ul>
        <li><Link to="/forms/react-form">react-form</Link></li>
        <li><Link to="/forms/formik">formik</Link></li>
      </ul>
    </div>

    {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
  </div>
);
