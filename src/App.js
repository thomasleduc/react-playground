import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Link,
} from 'react-router-dom';
import FormsPage from './pages/forms-page/FormsPage';
import FormikPage from './pages/formik-page/FormikPage';
import ReactFormPage from './pages/react-form-page/ReactFormPage';
import {RouteWithSubRoutes} from './utils/router';

const Home = () => <h2><Link to="/forms">react-form</Link></h2>;
const Header = () => (
  <header className="App-header">
    <img src={logo} className="App-logo" alt="logo"/>
    <h1 className="App-title">Welcome to React</h1>
  </header>
);

const routes = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/forms',
    component: FormsPage,
    routes: [
      {
        path: '/forms/react-form',
        component: ReactFormPage,
      },
      {
        path: '/forms/formik',
        component: FormikPage,
      },
    ],
  },
];

const App = () => (
  <Router>
    <div className="App">
      <Header />
      <div className="App-body">
        {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
      </div>
    </div>
  </Router>
);

export default App;
