
export const validateHello = value => ({
  error: !value || !/Hello World/.test(value) ? 'Input must contain \'Hello World\'' : null,
  warning: !value || !/^Hello World$/.test(value) ? 'Input should equal just \'Hello World\'' : null,
  success: value && /Hello World/.test(value) ? 'Thanks for entering \'Hello World\'!' : null,
});
